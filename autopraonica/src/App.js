import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Homepage, Profile, Carwashes,CarwashDetails, Header, SignInAndSignUpPage,Service,BasicCargoTruck,
BasicMinivan,BasicRegular,BasicSUV,DeluxCargoTruck,DeluxMinivan,DeluxRegular,DeluxSUV,
UltimateCargoTruck,UltimateMinivan,UltimateRegular,UltimateSUV,
PlatinumCargoTruck,PlatinumMinivan,PlatinumRegular,PlatinumSUV,Reservations,ReservationsDetails,Odjava} from './components';
import { auth, createUserProfileDocument, firestore} from './firebase/firebase.utils';
import { setCurrentUser } from './redux/user/user.actions';

class App extends React.Component {
  constructor(){
    super();

    this.state = {
      podaci: null,
      reservations:null,
      CurrentUser: null
    };
  }

  unsubscribeFromAuth = null;

 componentDidMount() {
    firestore.collection('users').get()
    .then( snapshot =>{
      const skupipodatke = []
      snapshot.forEach( doc => {
        const data =doc.data()
        skupipodatke.push(data)
      })

      this.setState({podaci: skupipodatke})
    }
      );

      firestore.collection('bookings').get()
    .then( snapshot =>{
      const sakupipodatke = []
      snapshot.forEach( doc => {
        const data =doc.data()
        sakupipodatke.push(data)
      })

      this.setState({reservations: sakupipodatke})
    }
      );

    const {setCurrentUser} = this.props;

    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      
            if(userAuth){
              const userRef = await createUserProfileDocument(userAuth);

              userRef.onSnapshot(snapShot => {
              setCurrentUser( {
                    id: snapShot.id,
                    ...snapShot.data()
                  });
                });
            }
            
              setCurrentUser(userAuth);

            if(userAuth){
                  
              
                  this.setState({CurrentUser: userAuth.uid});

                  

                  const currentFile = firestore.doc(`users/${userAuth.uid}`);
                  const currentFileData = await currentFile.get();
                  this.setState({CurrentUser:currentFileData.data()});
            }

              
            
   });
   
    
    
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }


  render(){
    return (
      <div>
      <Header/>
        <Switch>
          <Route exact path='/' render = {() => 
          this.props.currentUser ? (
            <Homepage />
          ):
        (
          <Redirect to='/signin' />
        )} />

        
          <Route path='/profil' render={() => 
          this.props.currentUser ? (
            <Profile CurrentUser={this.state.CurrentUser} />
          ):
        (
          <Redirect to='/signin' />
        )}/>



          <Route exact path='/signin' render={() => 
          this.props.currentUser ? (
            <Redirect to='/' />
          ):
        (
            <SignInAndSignUpPage/>
        )}/>


        
        <Route exact path='/lokacije' render={() => 
          this.props.currentUser ? (
            <Carwashes carwashes={this.state.podaci}/>
          ):
        (
          <Redirect to='/signin' />
        )}/>
         
       

        <Route path='/lokacije/:id' render={() => 
          this.props.currentUser ? (
            <CarwashDetails carwashes={this.state.podaci}/>
          ):
        (
          <Redirect to='/signin' />
        )}/>
       

        <Route  exact path='/narudžbe' render={() => 
          this.props.currentUser ? (
            <Reservations reservations={this.state.reservations} CurrentUser={this.state.CurrentUser}  />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route path='/narudžbe/:id' render={() => 
          this.props.currentUser ? (
            <ReservationsDetails reservations={this.state.reservations} />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge' render={() => 
          this.props.currentUser ? (
            <Service CurrentUser={this.state.CurrentUser}/>
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/odjava' render={() => 
          this.props.currentUser ? (
            <Odjava/>
          ):
        (
          <Redirect to='/signin' />
        )}/>



        <Route exact path='/usluge/BasicSUV' render={() => 
          this.props.currentUser ? (
            <BasicSUV />
          ):
        (
          <Redirect to='/signin' />
        )}/>


        <Route exact path='/usluge/BasicMinivan' render={() => 
          this.props.currentUser ? (
            <BasicMinivan />
          ):
        (
          <Redirect to='/signin' />
        )}/>


        <Route exact path='/usluge/BasicRegular' render={() => 
          this.props.currentUser ? (
            <BasicRegular />
          ):
        (
          <Redirect to='/signin' />
        )}/>


        <Route exact path='/usluge/BasicCargoTruck' render={() => 
          this.props.currentUser ? (
            <BasicCargoTruck />
          ):
        (
          <Redirect to='/signin' />
        )}/>


        <Route exact path='/usluge/DeluxSUV' render={() => 
          this.props.currentUser ? (
            <DeluxSUV />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/DeluxMinivan' render={() => 
          this.props.currentUser ? (
            <DeluxMinivan />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/DeluxCargoTruck' render={() => 
          this.props.currentUser ? (
            <DeluxCargoTruck />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/DeluxRegular' render={() => 
          this.props.currentUser ? (
            <DeluxRegular />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/UltimateSUV' render={() => 
          this.props.currentUser ? (
            <UltimateSUV />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/UltimateMinivan' render={() => 
          this.props.currentUser ? (
            <UltimateMinivan />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/UltimateCargoTruck' render={() => 
          this.props.currentUser ? (
            <UltimateCargoTruck />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/UltimateRegular' render={() => 
          this.props.currentUser ? (
            <UltimateRegular />
          ):
        (
          <Redirect to='/signin' />
        )}>
          
        </Route>
        <Route exact path='/usluge/PlatinumSUV'  render={() => 
          this.props.currentUser ? (
            <PlatinumSUV />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/PlatinumMinivan' render={() => 
          this.props.currentUser ? (
            <PlatinumMinivan />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/PlatinumCargoTruck' render={() => 
          this.props.currentUser ? (
            <PlatinumCargoTruck />
          ):
        (
          <Redirect to='/signin' />
        )}/>

        <Route exact path='/usluge/PlatinumRegular' render={() => 
          this.props.currentUser ? (
            <PlatinumRegular />
          ):
        (
          <Redirect to='/signin' />
        )} />
        
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser
})

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
});

export default connect( 
  mapStateToProps,
  mapDispatchToProps, 
)(App);
