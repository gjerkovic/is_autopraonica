import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


const config = {
    apiKey: "AIzaSyCoEnQ9jbnDXmRtKg471ArRGju5ckFT0Ps",
    authDomain: "autopraonica-db.firebaseapp.com",
    databaseURL: "https://autopraonica-db.firebaseio.com",
    projectId: "autopraonica-db",
    storageBucket: "autopraonica-db.appspot.com",
    messagingSenderId: "933940300296",
    appId: "1:933940300296:web:2fd8f5629f3dce26e99fbd",
    measurementId: "G-2YZTTZ1HY6"
  };

  export const  createUserProfileDocument = async(userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);

    const snapShot = await userRef.get();
    if(!snapShot.exists){
        const {displayName, email} = userAuth;
        const createdAt= new Date();

        try{
            await userRef.set(
                {
                    displayName,
                    email,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }

    return userRef;
  };

  export const addBooking = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`bookings/${userAuth.phonenumber}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,phonenumber} = userAuth;
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    phonenumber,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else {
        const {displayName, email,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }

    return servicesRef;

  }



  export const addBasicSUV = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`BasicSUV/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addDeluxSUV = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`DeluxSUV/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addUltimateSUV = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`UltimateSUV/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addPlatinumSUV = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`PlatinumSUV/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }


  export const addBasicMinivan = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`BasicMinivan/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addDeluxMinivan = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`DeluxMinivan/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addUltimateMinivan = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`UltimateMinivan/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addPlatinumMinivan = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`PlatinumMinivan/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }


  export const addBasicCargoTruck = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`BasicCargoTruck/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }


  export const addDeluxCargoTruck = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`DeluxCargoTruck/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }


  export const addUltimateCargoTruck = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`UltimateCargoTruck/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }


  export const addPlatinumCargoTruck = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`PlatinumCargoTruck/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }



  export const addBasicRegular = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`BasicRegular/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addDeluxRegular = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`DeluxRegular/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addUltimateRegular = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`UltimateRegular/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }

  export const addPlatinumRegular = async (userAuth, additionalData) => {
   

    const servicesRef = firestore.doc(`PlatinumRegular/${userAuth.OIB}`);
    const snapShot = await servicesRef.get();

    if(!snapShot.exists){
        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.set(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }
    }
    else{

        const {displayName, email,OIB,tvrtka,phonenumber} = userAuth;
        const createdAt= new Date();
        try{
            await servicesRef.update(
                {
                    displayName,
                    email,
                    OIB,
                    tvrtka,
                    phonenumber,
                    createdAt,
                    ...additionalData
                }
            )
        }catch (error){
            console.log('error vreating user', error.message );
        }

    }

    return servicesRef;

  }




  firebase.initializeApp(config);

  
  export const auth = firebase.auth();
  export const firestore = firebase.firestore();

  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({ prompt:'select_account' });
  export const signInWithGoogle = () => auth.signInWithPopup(provider);

  export default firebase;