import React from 'react';
import SignIn from '../../components/sign-in/sign-in.component';
import './sign-in-outpage.styles.scss';
import SignUp from '../../components/sign-up/sign-up.component';
import SignUpAdmin from '../../components/sign-up-admin/sign-up-admin.component'



const SignInAndSignUpPage = () =>(
    <div className='row'>
       
            <div className="column">
        <SignIn/>
        </div>
      
       
        <div className="column">
        <SignUp/>
        </div>
       
       
        <div className="column">
        <SignUpAdmin/>
        </div>
        
        <div className='column'>
            <h1> DOBRO DOŠLI <br/>
            AUTORPRAONICA</h1>
        </div>
        
    </div>
);

export default SignInAndSignUpPage;
