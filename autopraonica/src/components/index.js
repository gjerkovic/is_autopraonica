export { default as Profile } from './profil/profil.component.jsx';
export { default as Header } from './header/header.component.jsx';
export { default as SignIn } from './sign-in/sign-in.component.jsx';
export { default as SignUp } from './sign-up/sign-up.component.jsx';
export { default as CustomButton } from './CustomButton/CustomButton.jsx';
export { default as Carwashes } from './Carwashes/Carwashes.jsx';
export { default as Carwash } from './Carwashes/Carwash/Carwash.jsx';
export {default as Odjava} from './odjava/odjava';
export { default as Service } from './Services/Service/Service.jsx';
export {default as Reservations} from './Reservations/reservations';
export{default as ReservationsDetails} from './Reservations/ReservationsDetails';
export {default as BasicSUV } from './Services/Basic/BasicSUV/BasicSUV.jsx';
export {default as DeluxSUV } from './Services/Delux/DeluxSUV/DeluxSUV.jsx';
export {default as UltimateSUV } from './Services/Ultimate/UltimateSUV/UltimateSUV.jsx';
export {default as PlatinumSUV } from './Services/Platinum/PlatinumSUV/PlatinumSUV.jsx';

export {default as BasicMinivan } from './Services/Basic/BasicMinivan/BasicMinivan.jsx';
export {default as DeluxMinivan } from './Services/Delux/DeluxMinivan/DeluxMinivan.jsx';
export {default as UltimateMinivan } from './Services/Ultimate/UltimateMinivan/UltimateMinivan.jsx';
export {default as PlatinumMinivan } from './Services/Platinum/PlatinumMinivan/PlatinumMinivan.jsx';

export {default as BasicCargoTruck } from './Services/Basic/BasicCargoTruck/BasicCargoTruck.jsx';
export {default as DeluxCargoTruck } from './Services/Delux/DeluxCargoTruck/DeluxCargoTruck.jsx';
export {default as UltimateCargoTruck } from './Services/Ultimate/UltimateCargoTruck/UltimateCargoTruck.jsx';
export {default as PlatinumCargoTruck } from './Services/Platinum/PlatinumCargoTruck/PlatinumCargoTruck.jsx';

export {default as BasicRegular } from './Services/Basic/BasicRegular/BasicRegular.jsx';
export {default as DeluxRegular } from './Services/Delux/DeluxRegular/DeluxRegular.jsx';
export {default as UltimateRegular } from './Services/Ultimate/UltimateRegular/UltimateRegular.jsx';
export {default as PlatinumRegular } from './Services/Platinum/PlatinumRegular/PlatinumRegular.jsx';





export { default as Homepage } from '../pages/homepage/homepage.component.jsx';
export { default as SignInAndSignUpPage } from '../pages/sign-in-outpage/sign-in-outpage.component.jsx';
export {default as CarwashDetails} from './Carwashes/CarwashDetails.jsx';
export{default as Booking} from './Booking/Booking';