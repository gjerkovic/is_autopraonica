import React from 'react';
import './profil.styles.css';
import profile from "../../assets/3.jpg";

 const Profil = ({CurrentUser}) => {

    if(!CurrentUser) return 'Loading...';

return(
    <article className="c-card c-card--center">
    
    <header className="c-card__header">
    <img  src={profile}
     className="c-card__image" 
     alt="alt" />
    </header>

    <div className="c-card__body">
      <h2 className="c-card__title">
        
      </h2>
      <p className="c-card__intro">
        OIB: {CurrentUser.OIB}<br/>
        Tvrtka: {CurrentUser.tvrtka}<br/>
        Ime i Prezime: {CurrentUser.displayName}<br/>
        Email: {CurrentUser.email}<br/>
        Tel: {CurrentUser.phonenumber}
      </p>
    </div>
  </article>
  
  
)
     
 };

export default Profil;