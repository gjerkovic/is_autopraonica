import React from 'react';
import './directory.styles.css';
import MenuItem from '../menu-item/menu-item.component';
import photo1 from "../../assets/1.jpg";
import photo2 from "../../assets/2.jpg";
import photo3 from "../../assets/3.jpg";
import photo4 from "../../assets/4.jpg";
import photo5 from "../../assets/5.jpg";

class Directory extends React.Component{
    constructor(){
        super();

        this.state = {
            sections : [
                {
                  title: 'Autopraone',
                  imageUrl:photo1,
                  id: 1,
                  linkUrl: 'lokacije',
                  size: 'standard',
                  opis:'Pronadite svoju lokalnu autopraonu'
                  
                },
                {
                  title: 'Profil',
                  imageUrl: photo3,
                  id: 2,
                  linkUrl: 'profil',
                  size: 'standard',
                  opis:'Uredite svoj profil'
                },
                {
                  title: 'Vaše usluge',
                  imageUrl: photo4,
                  id: 3,
                  linkUrl: 'usluge',
                  size: 'standard',
                  opis:'Pogledajte sve svoje usluge'
                }
                ,
                {
                  title: 'Vaše narudžbe',
                  imageUrl: photo2,
                  id: 4,
                  linkUrl: 'narudžbe',
                  size: 'standard',
                  opis:'Pogledajte sve svoje narudžbe'
                },
                {
                  title: 'Odjava',
                  imageUrl: photo5,
                  id: 5,
                  linkUrl: 'odjava',
                  size: 'standard',
                  opis:'Odjava'
                }
              ]
              
        }



    }

    render(){
        return (
            <div className='directory-menu'>
              <section className="card-container">
                {this.state.sections.map(({id, ...otherSectionProps}) => (
                         <MenuItem key={id} {...otherSectionProps} />
                     )) }
               </section>
            </div>
        )
    }
}

export default Directory