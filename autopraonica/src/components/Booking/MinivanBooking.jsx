import  React from 'react'
import  './booking.css'
import {firestore,addBooking} from '../../firebase/firebase.utils';
import { withRouter } from "react-router-dom";
import DatePicker from '@bit/nexxtway.react-rainbow.date-picker';
import TimePicker from '@bit/nexxtway.react-rainbow.time-picker';
import { connect } from 'react-redux';
import  './booking.css'
import { BasicMinivan } from '..';







class MinivanBooking extends React.Component {
    constructor(){
        super();
        this.state = {

            Package:null,
            vehicletype:'Minivan',
            price:null,
            time:null,
            nameService:null,
            phoneService:null,
            date:new Date().toLocaleString(),
            resertime: new Date().toLocaleString(),
            currentPhoneNumber:null,


            BasicMinivan:null,
            DeluxMinivan:null,
            PlatinumMinivan:null,
            UltimateMinivan:null,
            choose:'Minivan'

        };
    }
    async  componentDidMount(){
           const idPage=this.props.match.params.id;
           /*Minivan*/
           const BasicMinivanRef = firestore.doc(`BasicMinivan/${idPage}`);
           const BasicMinivanData =  await BasicMinivanRef.get();
           this.setState({BasicMinivan:BasicMinivanData.data()});

           const DeluxMinivanRef = firestore.doc(`DeluxMinivan/${idPage}`);
           const DeluxMinivanData =  await DeluxMinivanRef.get();
           this.setState({DeluxMinivan:DeluxMinivanData.data()});

           const UltimateMinivanRef = firestore.doc(`UltimateMinivan/${idPage}`);
           const UltimateMinivanData =  await UltimateMinivanRef.get();
           this.setState({UltimateMinivan:UltimateMinivanData.data()});

           const PlatinumMinivanRef = firestore.doc(`PlatinumMinivan/${idPage}`);
           const PlatinumMinivanData =  await PlatinumMinivanRef.get();
           this.setState({PlatinumMinivan:PlatinumMinivanData.data()});

           
           
            
           
        }



        async  handleSubmit (){
    
            const {time,price,Package,resertime,date,phoneService,nameService,vehicletype,} = this.state;
            this.setState({nameService:BasicMinivan.tvrtka});
            this.setState({phoneService:BasicMinivan.phonenumber});
                try{
                    
    
                    await addBooking(this.props.currentUser, {time,price,Package,resertime,date,phoneService,nameService,vehicletype});
                    alert("USPJESNO")
                    
                } catch (error){
                    console.error(error);
                }
            
        }
    
    
    
    
    render(){

      const {BasicMinivan,DeluxMinivan,UltimateMinivan,PlatinumMinivan}=this.state;
      
      

      if (!(BasicMinivan && DeluxMinivan && UltimateMinivan && PlatinumMinivan)) return 'Autopraona nema jos uslugu za ovakav tip vozila';
        
        return (
        <div>
             

            <h1>Odabir paketa : Minivan</h1>
           <div className='row'>
            
                <div className="column">
                    <ul className="price">
                    <li className="header">Osnovni</li>
                    <li className="grey"> HRK {BasicMinivan.price}/ {BasicMinivan.time} min</li>
                    <li>{BasicMinivan.BasicOptionOne}</li>
                    <li>{BasicMinivan.BasicOptionTwo}</li>
                    <li>{BasicMinivan.BasicOptionThree}</li>
                    <li>{BasicMinivan.BasicOptionFour}</li>
                    <li>{BasicMinivan.BasicOptionFive}</li>
                    <li>{BasicMinivan.BasicOptionSix}</li>
                    <li>{BasicMinivan.BasicOptionSeven}</li>
                    <li>{BasicMinivan.BasicOptionEight}</li>
                    <li>{BasicMinivan.BasicOptionNine}</li>
                    <li>{BasicMinivan.BasicOptionTen}</li>
                    <li>{BasicMinivan.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Basic",
                        time:BasicMinivan.time,
                        price:BasicMinivan.price,
                        phoneService:BasicMinivan.phonenumber,
                        nameService:BasicMinivan.tvrtka
                })} className="button">Osnovni</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Delux</li>
                    <li className="grey">HRK {DeluxMinivan.price} / {DeluxMinivan.time} min</li>
                    <li>{DeluxMinivan.BasicOptionOne}</li>
                    <li>{DeluxMinivan.BasicOptionTwo}</li>
                    <li>{DeluxMinivan.BasicOptionThree}</li>
                    <li>{DeluxMinivan.BasicOptionFour}</li>
                    <li>{DeluxMinivan.BasicOptionFive}</li>
                    <li>{DeluxMinivan.BasicOptionSix}</li>
                    <li>{DeluxMinivan.BasicOptionSeven}</li>
                    <li>{DeluxMinivan.BasicOptionEight}</li>
                    <li>{DeluxMinivan.BasicOptionNine}</li>
                    <li>{DeluxMinivan.BasicOptionTen}</li>
                    <li>{DeluxMinivan.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Delux",
                        time:DeluxMinivan.time,
                        price:DeluxMinivan.price,
                        phoneService:BasicMinivan.phonenumber,
                        nameService:BasicMinivan.tvrtka
                })} className="button">Delux</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Ultimate</li>
                    <li className="grey">HRK{UltimateMinivan.price}  / {UltimateMinivan.time} min</li>
                    <li>{UltimateMinivan.BasicOptionOne}</li>
                    <li>{UltimateMinivan.BasicOptionTwo}</li>
                    <li>{UltimateMinivan.BasicOptionThree}</li>
                    <li>{UltimateMinivan.BasicOptionFour}</li>
                    <li>{UltimateMinivan.BasicOptionFive}</li>
                    <li>{UltimateMinivan.BasicOptionSix}</li>
                    <li>{UltimateMinivan.BasicOptionSeven}</li>
                    <li>{UltimateMinivan.BasicOptionEight}</li>
                    <li>{UltimateMinivan.BasicOptionNine}</li>
                    <li>{UltimateMinivan.BasicOptionTen}</li>
                    <li>{UltimateMinivan.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Ultimate",
                        time:UltimateMinivan.time,
                        price:UltimateMinivan.price,
                        phoneService:BasicMinivan.phonenumber,
                        nameService:BasicMinivan.tvrtka
                })} className="button">Ultimate</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Platinum</li>
                    <li className="grey">HRK {PlatinumMinivan.price} / {PlatinumMinivan.price} min</li>
                    <li>{PlatinumMinivan.BasicOptionOne}</li>
                    <li>{PlatinumMinivan.BasicOptionTwo}</li>
                    <li>{PlatinumMinivan.BasicOptionThree}</li>
                    <li>{PlatinumMinivan.BasicOptionFour}</li>
                    <li>{PlatinumMinivan.BasicOptionFive}</li>
                    <li>{PlatinumMinivan.BasicOptionSix}</li>
                    <li>{PlatinumMinivan.BasicOptionSeven}</li>
                    <li>{PlatinumMinivan.BasicOptionEight}</li>
                    <li>{PlatinumMinivan.BasicOptionNine}</li>
                    <li>{PlatinumMinivan.BasicOptionTen}</li>
                    <li>{PlatinumMinivan.BasicOptionEleven}</li>

                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Platinum",
                        time:PlatinumMinivan.time,
                        price:PlatinumMinivan.price,
                        phoneService:BasicMinivan.phonenumber,
                        nameService:BasicMinivan.tvrtka
                })} className="button">Platinum</button></li>
                    </ul>
                 </div>
                 
           </div>

            <div className='row'>
                   <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <DatePicker
                              value={this.state.date}
                              label="Odabir datuma"
                              onChange={value => this.setState({ date: value.toLocaleString })} 
                              />
                          </div>
                     </div> 

                     <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <TimePicker
                              value={this.state.resertime}
                              label="Odabir vremena"
                              onChange={value => this.setState({ resertime: value.toLocaleString() })} 
                              />
                          </div> 
                        </div>
                
            
            </div>
            <div className='row'>
            <div className="column"></div>
              <button className="button-reser" onClick={() => this.handleSubmit()}>REZERVIRAJ</button>
            </div>

        </div>
          );
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(MinivanBooking));