import  React from 'react'
import  './booking.css'
import {firestore,addBooking} from '../../firebase/firebase.utils';
import { withRouter } from "react-router-dom";
import DatePicker from '@bit/nexxtway.react-rainbow.date-picker';
import TimePicker from '@bit/nexxtway.react-rainbow.time-picker';
import { connect } from 'react-redux';
import  './booking.css'
import { BasicCargoTruck } from '..';







class CargoTruckBooking extends React.Component {
    constructor(){
        super();
        this.state = {

            Package:null,
            vehicletype:'CargoTruck',
            price:null,
            time:null,
            nameService:null,
            phoneService:null,
            date:new Date().toLocaleString(),
            resertime: new Date().toLocaleString(),
            currentPhoneNumber:null,


            BasicCargoTruck:null,
            DeluxCargoTruck:null,
            PlatinumCargoTruck:null,
            UltimateCargoTruck:null,
            choose:'CargoTruck'

        };
    }
    async  componentDidMount(){
           const idPage=this.props.match.params.id;
           /*CargoTruck*/
           const BasicCargoTruckRef = firestore.doc(`BasicCargoTruck/${idPage}`);
           const BasicCargoTruckData =  await BasicCargoTruckRef.get();
           this.setState({BasicCargoTruck:BasicCargoTruckData.data()});

           const DeluxCargoTruckRef = firestore.doc(`DeluxCargoTruck/${idPage}`);
           const DeluxCargoTruckData =  await DeluxCargoTruckRef.get();
           this.setState({DeluxCargoTruck:DeluxCargoTruckData.data()});

           const UltimateCargoTruckRef = firestore.doc(`UltimateCargoTruck/${idPage}`);
           const UltimateCargoTruckData =  await UltimateCargoTruckRef.get();
           this.setState({UltimateCargoTruck:UltimateCargoTruckData.data()});

           const PlatinumCargoTruckRef = firestore.doc(`PlatinumCargoTruck/${idPage}`);
           const PlatinumCargoTruckData =  await PlatinumCargoTruckRef.get();
           this.setState({PlatinumCargoTruck:PlatinumCargoTruckData.data()});

           
           
            
           
        }



        async  handleSubmit (){
    
            const {time,price,Package,resertime,date,phoneService,nameService,vehicletype,} = this.state;
            this.setState({nameService:BasicCargoTruck.tvrtka});
            this.setState({phoneService:BasicCargoTruck.phonenumber});
                try{
                    
    
                    await addBooking(this.props.currentUser, {time,price,Package,resertime,date,phoneService,nameService,vehicletype});
                    alert("USPJESNO")
                    
                } catch (error){
                    console.error(error);
                }
            
        }
    
    
    
    
    render(){

      const {BasicCargoTruck,DeluxCargoTruck,UltimateCargoTruck,PlatinumCargoTruck}=this.state;
      
      

      if (!(BasicCargoTruck && DeluxCargoTruck && UltimateCargoTruck && PlatinumCargoTruck)) return 'Autopraona nema jos uslugu za ovakav tip vozila';
        
        return (
        <div>
             

            <h1>Odabir paketa : CargoTruck</h1>
           <div className='row'>
            
                <div className="column">
                    <ul className="price">
                    <li className="header">Osnovni</li>
                    <li className="grey"> HRK {BasicCargoTruck.price}/ {BasicCargoTruck.time} min</li>
                    <li>{BasicCargoTruck.BasicOptionOne}</li>
                    <li>{BasicCargoTruck.BasicOptionTwo}</li>
                    <li>{BasicCargoTruck.BasicOptionThree}</li>
                    <li>{BasicCargoTruck.BasicOptionFour}</li>
                    <li>{BasicCargoTruck.BasicOptionFive}</li>
                    <li>{BasicCargoTruck.BasicOptionSix}</li>
                    <li>{BasicCargoTruck.BasicOptionSeven}</li>
                    <li>{BasicCargoTruck.BasicOptionEight}</li>
                    <li>{BasicCargoTruck.BasicOptionNine}</li>
                    <li>{BasicCargoTruck.BasicOptionTen}</li>
                    <li>{BasicCargoTruck.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Basic",
                        time:BasicCargoTruck.time,
                        price:BasicCargoTruck.price,
                        phoneService:BasicCargoTruck.phonenumber,
                        nameService:BasicCargoTruck.tvrtka
                })} className="button">Osnovni</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Delux</li>
                    <li className="grey">HRK {DeluxCargoTruck.price} / {DeluxCargoTruck.time} min</li>
                    <li>{DeluxCargoTruck.BasicOptionOne}</li>
                    <li>{DeluxCargoTruck.BasicOptionTwo}</li>
                    <li>{DeluxCargoTruck.BasicOptionThree}</li>
                    <li>{DeluxCargoTruck.BasicOptionFour}</li>
                    <li>{DeluxCargoTruck.BasicOptionFive}</li>
                    <li>{DeluxCargoTruck.BasicOptionSix}</li>
                    <li>{DeluxCargoTruck.BasicOptionSeven}</li>
                    <li>{DeluxCargoTruck.BasicOptionEight}</li>
                    <li>{DeluxCargoTruck.BasicOptionNine}</li>
                    <li>{DeluxCargoTruck.BasicOptionTen}</li>
                    <li>{DeluxCargoTruck.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Delux",
                        time:DeluxCargoTruck.time,
                        price:DeluxCargoTruck.price,
                        phoneService:BasicCargoTruck.phonenumber,
                        nameService:BasicCargoTruck.tvrtka
                })} className="button">Delux</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Ultimate</li>
                    <li className="grey">HRK{UltimateCargoTruck.price}  / {UltimateCargoTruck.time} min</li>
                    <li>{UltimateCargoTruck.BasicOptionOne}</li>
                    <li>{UltimateCargoTruck.BasicOptionTwo}</li>
                    <li>{UltimateCargoTruck.BasicOptionThree}</li>
                    <li>{UltimateCargoTruck.BasicOptionFour}</li>
                    <li>{UltimateCargoTruck.BasicOptionFive}</li>
                    <li>{UltimateCargoTruck.BasicOptionSix}</li>
                    <li>{UltimateCargoTruck.BasicOptionSeven}</li>
                    <li>{UltimateCargoTruck.BasicOptionEight}</li>
                    <li>{UltimateCargoTruck.BasicOptionNine}</li>
                    <li>{UltimateCargoTruck.BasicOptionTen}</li>
                    <li>{UltimateCargoTruck.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Ultimate",
                        time:UltimateCargoTruck.time,
                        price:UltimateCargoTruck.price,
                        phoneService:BasicCargoTruck.phonenumber,
                        nameService:BasicCargoTruck.tvrtka
                })} className="button">Ultimate</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Platinum</li>
                    <li className="grey">HRK {PlatinumCargoTruck.price} / {PlatinumCargoTruck.price} min</li>
                    <li>{PlatinumCargoTruck.BasicOptionOne}</li>
                    <li>{PlatinumCargoTruck.BasicOptionTwo}</li>
                    <li>{PlatinumCargoTruck.BasicOptionThree}</li>
                    <li>{PlatinumCargoTruck.BasicOptionFour}</li>
                    <li>{PlatinumCargoTruck.BasicOptionFive}</li>
                    <li>{PlatinumCargoTruck.BasicOptionSix}</li>
                    <li>{PlatinumCargoTruck.BasicOptionSeven}</li>
                    <li>{PlatinumCargoTruck.BasicOptionEight}</li>
                    <li>{PlatinumCargoTruck.BasicOptionNine}</li>
                    <li>{PlatinumCargoTruck.BasicOptionTen}</li>
                    <li>{PlatinumCargoTruck.BasicOptionEleven}</li>

                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Platinum",
                        time:PlatinumCargoTruck.time,
                        price:PlatinumCargoTruck.price,
                        phoneService:BasicCargoTruck.phonenumber,
                        nameService:BasicCargoTruck.tvrtka
                })} className="button">Platinum</button></li>
                    </ul>
                 </div>
                 
           </div>

            <div className='row'>
                   <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <DatePicker
                              value={this.state.date}
                              label="Odabir datuma"
                              onChange={value => this.setState({ date: value.toLocaleString() })} 
                              />
                          </div>
                     </div> 

                     <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <TimePicker
                              value={this.state.resertime}
                              label="Odabir vremena"
                              onChange={value => this.setState({ resertime: value.toLocaleString() })} 
                              />
                          </div> 
                        </div>
                
            
            </div>
            <div className='row'>
            <div className="column"></div>
              <button className="button-reser" onClick={() => this.handleSubmit()}>REZERVIRAJ</button>
            </div>

        </div>
          );
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(CargoTruckBooking));