import  React from 'react'
import  './booking.css'
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import SUVBooking  from './SUVBooking.jsx';
import RegularBooking from './RegularBooking.jsx';
import CargoTruckBooking from './CargoTruckBooking.jsx';
import MinivanBooking  from './MinivanBooking.jsx';
import SUV from "../../assets/SUV.png";
import Regular from "../../assets/Regular.png";
import Minivan from "../../assets/minivan.png";
import CargoTruck from "../../assets/cargotruck.png";






class Booking extends React.Component {
    constructor(){
        super();
        this.state = {

            choose:null

        };
    }
    
    render(){

      const {choose} = this.state;
      
      if (!choose) return (
        <div>
              <h1>Odabir tipa vozila </h1>
              <div className="row">
                <div className="column">
                  <div className="card" onClick={()=>this.setState({choose:'SUV'})}>
                  <h3>SUV</h3>
                  <img className="booking-img" alt="SUV" src={SUV} />
                    
                  </div>
                </div>
                <div className="column">
                  <div className="card" onClick={()=>this.setState({choose:'Regular'})}>
                    <h3>Regular</h3>
                    <img className="booking-img"  alt="Regular" src={Regular} />
                    
                  </div>
                </div>
                <div className="column">
                  <div className="card" onClick={()=>this.setState({choose:'Minivan'})} >
                    <h3>Minivan</h3>
                    <img className="booking-img"  alt="Minivan" src={Minivan} />
                  </div>
                </div>
                <div className="column">
                  <div className="card" onClick={()=>this.setState({choose:'CargoTruck'})}>
                    <h3>Kombi vozilo</h3>
                    <img className="booking-img"  alt="CargoTruck" src={CargoTruck} />
                  </div>
                </div>
            </div>
            </div>
          );

          else if(choose === 'SUV'){
            return (<SUVBooking/>)
          }
          else if(choose === 'Regular'){
            return <RegularBooking/>
          }
          else if(choose === 'CargoTruck'){
            return <CargoTruckBooking/>
          }
          else if(choose === 'Minivan'){
            return <MinivanBooking/>
          }
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(Booking));