import  React from 'react'
import  './booking.css'
import {firestore,addBooking} from '../../firebase/firebase.utils';
import { withRouter } from "react-router-dom";
import DatePicker from '@bit/nexxtway.react-rainbow.date-picker';
import TimePicker from '@bit/nexxtway.react-rainbow.time-picker';
import { connect } from 'react-redux';
import  './booking.css'
import { BasicSUV } from '..';







class SUVBooking extends React.Component {
    constructor(){
        super();
        this.state = {

            Package:null,
            vehicletype:'SUV',
            price:null,
            time:null,
            nameService:null,
            phoneService:null,
            date:new Date().toLocaleString(),
            resertime: new Date().toLocaleString(),
            currentPhoneNumber:null,


            BasicSUV:null,
            DeluxSUV:null,
            PlatinumSUV:null,
            UltimateSUV:null,
            choose:'SUV'

        };
    }
    async  componentDidMount(){
           const idPage=this.props.match.params.id;
           /*SUV*/
           const BasicSUVRef = firestore.doc(`BasicSUV/${idPage}`);
           const BasicSUVData =  await BasicSUVRef.get();
           this.setState({BasicSUV:BasicSUVData.data()});

           const DeluxSUVRef = firestore.doc(`DeluxSUV/${idPage}`);
           const DeluxSUVData =  await DeluxSUVRef.get();
           this.setState({DeluxSUV:DeluxSUVData.data()});

           const UltimateSUVRef = firestore.doc(`UltimateSUV/${idPage}`);
           const UltimateSUVData =  await UltimateSUVRef.get();
           this.setState({UltimateSUV:UltimateSUVData.data()});

           const PlatinumSUVRef = firestore.doc(`PlatinumSUV/${idPage}`);
           const PlatinumSUVData =  await PlatinumSUVRef.get();
           this.setState({PlatinumSUV:PlatinumSUVData.data()});

           
           
            
           
        }



        async  handleSubmit (){
    
            const {time,price,Package,resertime,date,phoneService,nameService,vehicletype,} = this.state;
            this.setState({nameService:BasicSUV.tvrtka});
            this.setState({phoneService:BasicSUV.phonenumber});
                try{
                    
    
                    await addBooking(this.props.currentUser, {time,price,Package,resertime,date,phoneService,nameService,vehicletype});
                    alert("USPJESNO")
                    
                } catch (error){
                    console.error(error);
                }
            
        }
    
    
    
    
    render(){

      const {BasicSUV,DeluxSUV,UltimateSUV,PlatinumSUV}=this.state;
      
      
      if(!this.props.currentUser) return "Lodaing...";
      if (!(BasicSUV && DeluxSUV && UltimateSUV && PlatinumSUV)) return 'Autopraona nema jos uslugu za ovakav tip vozila';
        
        return (
        <div>
             

            <h1>Odabir paketa : SUV</h1>
           <div className='row'>
            
                <div className="column">
                    <ul className="price">
                    <li className="header">Osnovni</li>
                    <li className="grey"> HRK {BasicSUV.price}/ {BasicSUV.time} min</li>
                    <li>{BasicSUV.BasicOptionOne}</li>
                    <li>{BasicSUV.BasicOptionTwo}</li>
                    <li>{BasicSUV.BasicOptionThree}</li>
                    <li>{BasicSUV.BasicOptionFour}</li>
                    <li>{BasicSUV.BasicOptionFive}</li>
                    <li>{BasicSUV.BasicOptionSix}</li>
                    <li>{BasicSUV.BasicOptionSeven}</li>
                    <li>{BasicSUV.BasicOptionEight}</li>
                    <li>{BasicSUV.BasicOptionNine}</li>
                    <li>{BasicSUV.BasicOptionTen}</li>
                    <li>{BasicSUV.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Basic",
                        time:BasicSUV.time,
                        price:BasicSUV.price,
                        phoneService:BasicSUV.phonenumber,
                        nameService:BasicSUV.tvrtka
                })} className="button">Osnovni</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Delux</li>
                    <li className="grey">HRK {DeluxSUV.price} / {DeluxSUV.time} min</li>
                    <li>{DeluxSUV.BasicOptionOne}</li>
                    <li>{DeluxSUV.BasicOptionTwo}</li>
                    <li>{DeluxSUV.BasicOptionThree}</li>
                    <li>{DeluxSUV.BasicOptionFour}</li>
                    <li>{DeluxSUV.BasicOptionFive}</li>
                    <li>{DeluxSUV.BasicOptionSix}</li>
                    <li>{DeluxSUV.BasicOptionSeven}</li>
                    <li>{DeluxSUV.BasicOptionEight}</li>
                    <li>{DeluxSUV.BasicOptionNine}</li>
                    <li>{DeluxSUV.BasicOptionTen}</li>
                    <li>{DeluxSUV.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Delux",
                        time:DeluxSUV.time,
                        price:DeluxSUV.price,
                        phoneService:BasicSUV.phonenumber,
                        nameService:BasicSUV.tvrtka
                })} className="button">Delux</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Ultimate</li>
                    <li className="grey">HRK{UltimateSUV.price}  / {UltimateSUV.time} min</li>
                    <li>{UltimateSUV.BasicOptionOne}</li>
                    <li>{UltimateSUV.BasicOptionTwo}</li>
                    <li>{UltimateSUV.BasicOptionThree}</li>
                    <li>{UltimateSUV.BasicOptionFour}</li>
                    <li>{UltimateSUV.BasicOptionFive}</li>
                    <li>{UltimateSUV.BasicOptionSix}</li>
                    <li>{UltimateSUV.BasicOptionSeven}</li>
                    <li>{UltimateSUV.BasicOptionEight}</li>
                    <li>{UltimateSUV.BasicOptionNine}</li>
                    <li>{UltimateSUV.BasicOptionTen}</li>
                    <li>{UltimateSUV.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Ultimate",
                        time:UltimateSUV.time,
                        price:UltimateSUV.price,
                        phoneService:BasicSUV.phonenumber,
                        nameService:BasicSUV.tvrtka
                })} className="button">Ultimate</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Platinum</li>
                    <li className="grey">HRK {PlatinumSUV.price} / {PlatinumSUV.price} min</li>
                    <li>{PlatinumSUV.BasicOptionOne}</li>
                    <li>{PlatinumSUV.BasicOptionTwo}</li>
                    <li>{PlatinumSUV.BasicOptionThree}</li>
                    <li>{PlatinumSUV.BasicOptionFour}</li>
                    <li>{PlatinumSUV.BasicOptionFive}</li>
                    <li>{PlatinumSUV.BasicOptionSix}</li>
                    <li>{PlatinumSUV.BasicOptionSeven}</li>
                    <li>{PlatinumSUV.BasicOptionEight}</li>
                    <li>{PlatinumSUV.BasicOptionNine}</li>
                    <li>{PlatinumSUV.BasicOptionTen}</li>
                    <li>{PlatinumSUV.BasicOptionEleven}</li>

                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Platinum",
                        time:PlatinumSUV.time,
                        price:PlatinumSUV.price,
                        phoneService:BasicSUV.phonenumber,
                        nameService:BasicSUV.tvrtka
                })} className="button">Platinum</button></li>
                    </ul>
                 </div>
                 
           </div>

            <div className='row'>
                   <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <DatePicker
                              value={this.state.date}
                              label="Odabir datuma"
                              onChange={value => this.setState({ date: value.toLocaleString() })} 
                              />
                          </div>
                     </div> 

                     <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <TimePicker
                              value={this.state.resertime}
                              label="Odabir vremena"
                              onChange={value => this.setState({ resertime: value.toLocaleString() })} 
                              />
                          </div> 
                        </div>
                
            
            </div>
            <div className='row'>
            <div className="column"></div>
              <button className="button-reser" onClick={() => this.handleSubmit()}>REZERVIRAJ</button>
            </div>

        </div>
          );
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(SUVBooking));