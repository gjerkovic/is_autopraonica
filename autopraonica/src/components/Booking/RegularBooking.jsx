import  React from 'react'
import  './booking.css'
import {firestore,addBooking} from '../../firebase/firebase.utils';
import { withRouter } from "react-router-dom";
import DatePicker from '@bit/nexxtway.react-rainbow.date-picker';
import TimePicker from '@bit/nexxtway.react-rainbow.time-picker';
import { connect } from 'react-redux';
import  './booking.css'
import { BasicRegular } from '..';







class RegularBooking extends React.Component {
    constructor(){
        super();
        this.state = {

            Package:null,
            vehicletype:'Regular',
            price:null,
            time:null,
            nameService:null,
            phoneService:null,
            date:new Date().toLocaleString(),
            resertime: new Date().toLocaleString(),
            currentPhoneNumber:null,


            BasicRegular:null,
            DeluxRegular:null,
            PlatinumRegular:null,
            UltimateRegular:null,
            choose:'Regular'

        };
    }
    async  componentDidMount(){
           const idPage=this.props.match.params.id;
           /*Regular*/
           const BasicRegularRef = firestore.doc(`BasicRegular/${idPage}`);
           const BasicRegularData =  await BasicRegularRef.get();
           this.setState({BasicRegular:BasicRegularData.data()});

           const DeluxRegularRef = firestore.doc(`DeluxRegular/${idPage}`);
           const DeluxRegularData =  await DeluxRegularRef.get();
           this.setState({DeluxRegular:DeluxRegularData.data()});

           const UltimateRegularRef = firestore.doc(`UltimateRegular/${idPage}`);
           const UltimateRegularData =  await UltimateRegularRef.get();
           this.setState({UltimateRegular:UltimateRegularData.data()});

           const PlatinumRegularRef = firestore.doc(`PlatinumRegular/${idPage}`);
           const PlatinumRegularData =  await PlatinumRegularRef.get();
           this.setState({PlatinumRegular:PlatinumRegularData.data()});

           
           
            
           
        }



        async  handleSubmit (){
    
            const {time,price,Package,resertime,date,phoneService,nameService,vehicletype,} = this.state;
            this.setState({nameService:BasicRegular.tvrtka});
            this.setState({phoneService:BasicRegular.phonenumber});
                try{
                    
    
                    await addBooking(this.props.currentUser, {time,price,Package,resertime,date,phoneService,nameService,vehicletype});
                    alert("USPJESNO")
                    
                } catch (error){
                    console.error(error);
                }
            
        }
    
    
    
    
    render(){

      const {BasicRegular,DeluxRegular,UltimateRegular,PlatinumRegular}=this.state;
      
      

      if (!(BasicRegular && DeluxRegular && UltimateRegular && PlatinumRegular)) return 'Autopraona nema jos uslugu za ovakav tip vozila';
        
        return (
        <div>
             

            <h1>Odabir paketa : Regular</h1>
           <div className='row'>
            
                <div className="column">
                    <ul className="price">
                    <li className="header">Osnovni</li>
                    <li className="grey"> HRK {BasicRegular.price}/ {BasicRegular.time} min</li>
                    <li>{BasicRegular.BasicOptionOne}</li>
                    <li>{BasicRegular.BasicOptionTwo}</li>
                    <li>{BasicRegular.BasicOptionThree}</li>
                    <li>{BasicRegular.BasicOptionFour}</li>
                    <li>{BasicRegular.BasicOptionFive}</li>
                    <li>{BasicRegular.BasicOptionSix}</li>
                    <li>{BasicRegular.BasicOptionSeven}</li>
                    <li>{BasicRegular.BasicOptionEight}</li>
                    <li>{BasicRegular.BasicOptionNine}</li>
                    <li>{BasicRegular.BasicOptionTen}</li>
                    <li>{BasicRegular.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Basic",
                        time:BasicRegular.time,
                        price:BasicRegular.price,
                        phoneService:BasicRegular.phonenumber,
                        nameService:BasicRegular.tvrtka
                })} className="button">Osnovni</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Delux</li>
                    <li className="grey">HRK {DeluxRegular.price} / {DeluxRegular.time} min</li>
                    <li>{DeluxRegular.BasicOptionOne}</li>
                    <li>{DeluxRegular.BasicOptionTwo}</li>
                    <li>{DeluxRegular.BasicOptionThree}</li>
                    <li>{DeluxRegular.BasicOptionFour}</li>
                    <li>{DeluxRegular.BasicOptionFive}</li>
                    <li>{DeluxRegular.BasicOptionSix}</li>
                    <li>{DeluxRegular.BasicOptionSeven}</li>
                    <li>{DeluxRegular.BasicOptionEight}</li>
                    <li>{DeluxRegular.BasicOptionNine}</li>
                    <li>{DeluxRegular.BasicOptionTen}</li>
                    <li>{DeluxRegular.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Delux",
                        time:DeluxRegular.time,
                        price:DeluxRegular.price,
                        phoneService:BasicRegular.phonenumber,
                        nameService:BasicRegular.tvrtka
                })} className="button">Delux</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Ultimate</li>
                    <li className="grey">HRK{UltimateRegular.price}  / {UltimateRegular.time} min</li>
                    <li>{UltimateRegular.BasicOptionOne}</li>
                    <li>{UltimateRegular.BasicOptionTwo}</li>
                    <li>{UltimateRegular.BasicOptionThree}</li>
                    <li>{UltimateRegular.BasicOptionFour}</li>
                    <li>{UltimateRegular.BasicOptionFive}</li>
                    <li>{UltimateRegular.BasicOptionSix}</li>
                    <li>{UltimateRegular.BasicOptionSeven}</li>
                    <li>{UltimateRegular.BasicOptionEight}</li>
                    <li>{UltimateRegular.BasicOptionNine}</li>
                    <li>{UltimateRegular.BasicOptionTen}</li>
                    <li>{UltimateRegular.BasicOptionEleven}</li>
                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Ultimate",
                        time:UltimateRegular.time,
                        price:UltimateRegular.price,
                        phoneService:BasicRegular.phonenumber,
                        nameService:BasicRegular.tvrtka
                })} className="button">Ultimate</button></li>
                    </ul>
                 </div>
                 <div className="column">
                    <ul className="price">
                    <li className="header">Platinum</li>
                    <li className="grey">HRK {PlatinumRegular.price} / {PlatinumRegular.price} min</li>
                    <li>{PlatinumRegular.BasicOptionOne}</li>
                    <li>{PlatinumRegular.BasicOptionTwo}</li>
                    <li>{PlatinumRegular.BasicOptionThree}</li>
                    <li>{PlatinumRegular.BasicOptionFour}</li>
                    <li>{PlatinumRegular.BasicOptionFive}</li>
                    <li>{PlatinumRegular.BasicOptionSix}</li>
                    <li>{PlatinumRegular.BasicOptionSeven}</li>
                    <li>{PlatinumRegular.BasicOptionEight}</li>
                    <li>{PlatinumRegular.BasicOptionNine}</li>
                    <li>{PlatinumRegular.BasicOptionTen}</li>
                    <li>{PlatinumRegular.BasicOptionEleven}</li>

                    <li className="grey"><button onClick={()=> this.setState({
                        Package:"Platinum",
                        time:PlatinumRegular.time,
                        price:PlatinumRegular.price,
                        phoneService:BasicRegular.phonenumber,
                        nameService:BasicRegular.tvrtka
                })} className="button">Platinum</button></li>
                    </ul>
                 </div>
                 
           </div>

            <div className='row'>
                   <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <DatePicker
                              value={this.state.date}
                              label="Odabir datuma"
                              onChange={value => this.setState({ date: value.toLocaleString() })} 
                              />
                          </div>
                     </div> 

                     <div className='column-custom'>
                          <div className="rainbow-p-vertical_large rainbow-p-horizontal_xx-large rainbow-m-horizontal_xx-large">
                            <TimePicker
                              value={this.state.resertime}
                              label="Odabir vremena"
                              onChange={value => this.setState({ resertime: value.toLocaleString() })} 
                              />
                          </div> 
                        </div>
                
            
            </div>
            
            <div className='row'>
            <div className="column"></div>
              <button className="button-reser" onClick={() => this.handleSubmit()}>REZERVIRAJ</button>
              
            </div>

        </div>
          );
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(RegularBooking));