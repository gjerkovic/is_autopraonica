import React from 'react';
import profile from '../../assets/5.jpg';
import { connect } from 'react-redux';
import { auth } from '../../firebase/firebase.utils';

 const Odjava = ({currentUser}) => {

    if(!currentUser) return 'Loading...';

return(
    <article className="c-card c-card--center" onClick= {()=> auth.signOut()}>
    
    <header className="c-card__header">
    <img  src={profile}
     className="c-card__image" 
     alt="alt" />
    </header>

    <div className="c-card__body">
      <h2 className="c-card__title">
        KLIKNITE NA KARTICU ZA JEDINSTVENU ODJAVU 
      </h2>
      <p className="c-card__intro">
        
      </p>
    </div>
  </article>
  
  
)
     
 };

 const mapSateToProps = (state) => ({
    currentUser: state.user.currentUser
});


export default connect(mapSateToProps)(Odjava);