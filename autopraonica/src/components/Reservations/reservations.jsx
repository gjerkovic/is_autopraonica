import React from 'react';
import Reservation from './reservation/reservation';

 const Reservations = ({CurrentUser,reservations}) => {

if(!CurrentUser) return 'Loading...';
if(!reservations) return 'Loading...';
console.log(reservations)
return  reservations.map((reservation) => (reservation.phonenumber===CurrentUser.phonenumber || reservation.phoneService===CurrentUser.phonenumber) && <Reservation podaci={reservation}/>);
  
     
 };

export default Reservations;