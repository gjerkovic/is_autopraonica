import  React from 'react'
import  '../Booking/booking.css'
import {firestore} from '../../firebase/firebase.utils';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import photo1 from '../../assets/7.jpg';
import photo4 from '../../assets/10.jpg';
import photo5 from '../../assets/11.jpg';
import photo6 from '../../assets/12.jpg';
import photo7 from '../../assets/1.jpg';









class Reserved extends React.Component {
    constructor(){
        super();
        this.state = {
            yourreservation:null,
            

        };
    }
    async  componentDidMount(){
           const idPage=this.props.match.params.id;
           
           const document = firestore.doc(`bookings/${idPage}`);
           const filedata =  await document.get();
           this.setState({yourreservation:filedata.data()});
           
           
            
           
        }



    
    
    render(){

        const {yourreservation}=this.state
        if(!yourreservation) return 'Loading';

        return (
            <div>

           
            <h1>VAŠA REZERVACIJA</h1>
            <Link to={`/lokacije`}>
            <article className="c-card c-card--center">
            <header className="c-card__header">
              <img className="c-card__image" alt="alt" src={photo4} />
            </header>    
            <div className="c-card__body">
              <h2 className="c-card__title">
        Paket: {yourreservation.vehicletype} {yourreservation.Package}
              </h2>
            <p className="c-card__intro" style={{color: "white"}}>
               
              </p>
            </div>    
          </article>
          </Link> 
          <Link to={`/lokacije`}>
            <article className="c-card c-card--center">
            <header className="c-card__header">
              <img className="c-card__image" alt="alt" src={photo1}/>
            </header>    
            <div className="c-card__body">
              <h2 className="c-card__title">
                Datum: {yourreservation.date} <br/>
                Vrijeme: {yourreservation.resertime}
              </h2>
            <p className="c-card__intro" style={{color: "white"}}>
               
              </p>
            </div>    
          </article>
          </Link> 
          <Link to={`/lokacije`}>
            <article className="c-card c-card--center">
            <header className="c-card__header">
              <img className="c-card__image" alt="alt" src={photo7} />
            </header>    
            <div className="c-card__body">
              <h2 className="c-card__title">
                Autopraonica: {yourreservation.nameService}
              </h2>
            <p className="c-card__intro" style={{color: "white"}}>
               
              </p>
            </div>    
          </article>
          </Link> 
          <Link to={`/lokacije`}>
            <article className="c-card c-card--center">
            <header className="c-card__header">
              <img className="c-card__image" alt="alt" src={photo5} />
            </header>    
            <div className="c-card__body">
              <h2 className="c-card__title">
                Vrijeme trajanja: {yourreservation.time} min
              </h2>
            <p className="c-card__intro" style={{color: "white"}}>
               
              </p>
            </div>    
          </article>
          </Link> 
          <Link to={`/lokacije`}>
            <article className="c-card c-card--center">
            <header className="c-card__header">
              <img className="c-card__image" alt="alt" src={photo6} />
            </header>    
            <div className="c-card__body">
              <h2 className="c-card__title">
                Iznos: {yourreservation.price} HRK
              </h2>
            <p className="c-card__intro" style={{color: "white"}}>
               
              </p>
            </div>    
          </article>
          </Link> 

          </div>
          );
    }
}



const mapSateToProps = (state) => ({
  currentUser: state.user.currentUser
});


export default connect(mapSateToProps) (withRouter(Reserved));