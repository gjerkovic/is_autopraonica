import React from 'react';
import Reserved from './Reserved.jsx';
import { useParams } from "react-router-dom";

const ReservationsDetails = ({ reservations } ) => {
    const { id } = useParams();
   console.log(id)
    let chosenResrvation = null;

    if(reservations) {
        chosenResrvation = reservations.find((reservation) => reservation.phonenumber === id);
    } 
    
    if(!chosenResrvation) {
      return 'Loading...';
    }

    return (
         
         <Reserved/>
        
      );
}

export default ReservationsDetails;