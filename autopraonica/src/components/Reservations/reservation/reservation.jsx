import React from 'react';
import {Link} from 'react-router-dom';
import photo from "../../../assets/6.jpg";

const Reservation = (props) => {
  if(!props.podaci) return "Loading..." ;
      
  return (
    
        <Link to={`/narudžbe/${props.podaci.phonenumber}`}>
          <article className="c-card c-card--center">
          <header className="c-card__header">
            <img className="c-card__image" alt="alt" src={photo} />
          </header>    
          <div className="c-card__body">
            <h2 className="c-card__title">
              {props.podaci.nameService}
            </h2>
          <p className="c-card__intro" style={{color: "white"}}>
              Datum rezervacije: {props.podaci.date}<br/>
              Vrijeme rezervacije: {props.podaci.resertime}<br/>
              Cijena rezervacije: {props.podaci.price}<br/>
            </p>
          </div>    
        </article>
        </Link> 
      );
}

export default Reservation;