import React from 'react';
import { shallow, mount, render} from 'enzyme';
import MenuItem from './menu-item.component';

it('expect to render MenuItem component',() => {
    expect(shallow(<MenuItem/>).length).toEqual(1)
})