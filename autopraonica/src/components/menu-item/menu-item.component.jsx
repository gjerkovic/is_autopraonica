import React from 'react';
import {withRouter} from 'react-router-dom';
import './menu-item.styels.css';



const MenuItem = ({title, imageUrl, history, linkUrl, match, opis}) => {
    
    return (
    <article className="c-card c-card--center" onClick={() => history.push(`${match.url}${linkUrl}`)}>
    
  <header className="c-card__header">
    <img  src={imageUrl}
     className="c-card__image" 
     alt="alt" />
  </header>

  <div className="c-card__body">
    <h2 className="c-card__title">
      {title}
    </h2>
   <p className="c-card__intro">
       {opis} 
    </p>
  </div>

</article>);





};

export default withRouter(MenuItem);