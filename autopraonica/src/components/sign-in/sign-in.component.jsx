import React from 'react';
import FormInput from '../form-input/form-input.component';

import { CustomButton} from '../';
import './sign-in.styles.css';
import {auth} from '../../firebase/firebase.utils';



class SignIn extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            email:'',
            password:''
        }
    }

    handleSubmit =async event => {
        event.preventDefault();

        const{email,password} = this.state;

        try{
            await auth.signInWithEmailAndPassword(email,password);
            this.setState({email:'', password: ''});
        }catch(error){
            console.log(error);
        }

        

    }

    handleChange = event => {
        const {value,name} = event.target;
        this.setState({ [name]:value })
    }

    render(){
        const {email,password} = this.state;
        return(
            <div className='sign-in'>

                <h2>PRIJAVA</h2>
                <form className='sign-up-form'  onSubmit={this.handleSubmit}>
                    <FormInput 
                    name="email"
                     type="email"
                      value={email} 
                      handleChange={this.handleChange}
                      lable="email"
                      placeholder="email"
                      required />
                    <FormInput 
                    name="password"
                     type="password" 
                     value={password} 
                     handleChange={this.handleChange}
                     lable="password"
                     placeholder="password"
                     required />

                    <CustomButton type="submit">Sign In</CustomButton>
                    
                </form>
            </div>
        )
    }
}

export default SignIn;