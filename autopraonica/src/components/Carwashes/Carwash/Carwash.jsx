import React from 'react';
import {Link} from 'react-router-dom';
import photo from "../../../assets/6.jpg";

const Carwash = (props) => {
  if(!props.podaci.OIB) return "Loading..." ;
      
  return (
    
        <Link to={`/lokacije/${props.podaci.OIB}`}>
          <article className="c-card c-card--center">
          <header className="c-card__header">
            <img className="c-card__image" alt="alt" src={photo} />
          </header>    
          <div className="c-card__body">
            <h2 className="c-card__title">
              {props.podaci.tvrtka}
            </h2>
          <p className="c-card__intro" style={{color: "white"}}>
              {props.podaci.dispayName}<br/>
              {props.podaci.adress}<br/>
              {props.podaci.phonenumber}<br/>
            </p>
          </div>    
        </article>
        </Link> 
      );
}

export default Carwash;