import React from 'react';

import Carwash from './Carwash/Carwash.jsx';

const Carwashes = ({ carwashes }) => {
    if(!carwashes) return 'Loading...';
    
    return carwashes.map((carwash) => carwash.admin && <Carwash key={carwash.OIB} podaci={carwash}/>);
}

export default Carwashes;