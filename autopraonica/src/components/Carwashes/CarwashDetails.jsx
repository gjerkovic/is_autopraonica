import React from 'react';
import Booking from '../Booking/Booking';
import { useParams } from "react-router-dom";

const CarwashDetails = ({ carwashes } ) => {
    const { id } = useParams();
   
    let chosenCarwash = null;

    if(carwashes) {
      chosenCarwash = carwashes.find((carwash) => carwash.OIB === id);
    } 
    
    if(!chosenCarwash) {
      return 'Loading...';
    }

    return (
         
          <Booking/>
        
      );
}

export default CarwashDetails;