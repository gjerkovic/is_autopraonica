import React from 'react';
import {addUltimateMinivan} from '../../../../firebase/firebase.utils';
import { connect } from 'react-redux';
import './UltimateMinivan.css';

class UltimateMinivan extends React.Component{
    constructor(){
        super();
                this.state={
                
                    BasicOptionOne:'Vanjsko pranje',
                    BasicOptionTwo:'Unutarnje pranje',
                    BasicOptionThree:'Vosak',
                    BasicOptionFour:'Detaljno pranje naplataka',
                    BasicOptionFive:'Zaštitni premaz kože',
                    BasicOptionSix:'Osvježavanje ventilacijskog sustava',
                    BasicOptionSeven:'Gel premaz guma',
                    BasicOptionEight:'/',
                    BasicOptionNine:'/',
                    BasicOptionTen:'/',
                    BasicOptionEleven:'/',
                    time:'/',
                    price:'/'
                    
                    

                }
                    
                }

            
                handleSubmit = async event => {
                    event.preventDefault();

                    const {BasicOptionOne, BasicOptionTwo, BasicOptionThree,BasicOptionFour,BasicOptionFive,BasicOptionSix,
                    BasicOptionSeven,
                    BasicOptionEight,
                    BasicOptionNine,
                    BasicOptionTen,
                    BasicOptionEleven,
                    time,
                    price} = this.state;

                
                        try{

                            await addUltimateMinivan(this.props.currentUser, {BasicOptionOne, BasicOptionTwo, BasicOptionThree,BasicOptionFour,BasicOptionFive,BasicOptionSix,
                                BasicOptionSeven,
                                BasicOptionEight,
                                BasicOptionNine,
                                BasicOptionTen,
                                BasicOptionEleven,
                                time,
                                price});

                                  alert("Uspjesno")  ;
                        } 
                    catch (error){
                        console.error(error);
                    }
       
        }


    handleChange = event =>{
        const {name,value} = event.target;
      
        this.setState({[name]: value});
        
    }

    render(){
      const  {BasicOptionOne, BasicOptionTwo, BasicOptionThree,BasicOptionFour,BasicOptionFive,BasicOptionSix,
        BasicOptionSeven,
        BasicOptionEight,
        BasicOptionNine,
        BasicOptionTen,
        BasicOptionEleven,
        time,
        price} = this.state;
        return(
            
            <div className="row">
            <div className="col-75">
              <div className="container">
                <form onSubmit={this.handleSubmit}>
                  <div className="row">
                    <div className="col-50">
                      <h3>Minivan: Ultimate</h3>
                      <label htmlFor="fname"><i className="fa fa-user"/> Usluga 1</label>
                      <input type="text" id="fname" name='BasicOptionOne' value={BasicOptionOne} onChange={this.handleChange} placeholder='Unutarnje pranje' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 2</label>
                      <input type="text" id="fname" name='BasicOptionTwo' value={BasicOptionTwo}  onChange={this.handleChange} placeholder='Vanjsko pranje' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 3</label>
                      <input type="text" id="fname" name='BasicOptionThree' value={BasicOptionThree}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 4</label>
                      <input type="text" id="fname" name='BasicOptionFour' value={BasicOptionFour}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 5</label>
                      <input type="text" id="fname" name='BasicOptionFive' value={BasicOptionFive}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 6</label>
                      <input type="text" id="fname" name='BasicOptionSix' value={BasicOptionSix}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 7</label>
                      <input type="text" id="fname" name='BasicOptionSeven' value={BasicOptionSeven}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 8</label>
                      <input type="text" id="fname" name='BasicOptionEight' value={BasicOptionEight}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 9</label>
                      <input type="text" id="fname" name='BasicOptionNine' value={BasicOptionNine}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 10</label>
                      <input type="text" id="fname" name='BasicOptionTen' value={BasicOptionTen}  onChange={this.handleChange} placeholder='/' />
                      <label htmlFor="fname"><i className="fa fa-user" /> Usluga 11</label>
                      <input type="text" id="fname" name='BasicOptionEleven' value={BasicOptionEleven}  onChange={this.handleChange} placeholder='/' />
                      
                    </div>
                    <div className="col-50">
                      <h3>Cijena i vrijeme trajanja</h3>
                      <label htmlFor="fname">Accepted Cards</label>
                      <div className="icon-container">
                        <i className="fa fa-cc-visa" style={{color: 'navy'}} />
                        <i className="fa fa-cc-amex" style={{color: 'blue'}} />
                        <i className="fa fa-cc-mastercard" style={{color: 'red'}} />
                        <i className="fa fa-cc-discover" style={{color: 'orange'}} />
                      </div>
                      <label htmlFor="cname">Ukupna cijena paketa Minivan: Ultimate</label>
                      <input type="text" id="cname" name='price' value= {price}onChange={this.handleChange} placeholder="HRK" />
                      <label htmlFor="ccnum">Ukupno vrijeme trajanja paketa Minivan: Ultimate</label>
                      <input type="text" id="ccnum" name='time'value={time} onChange={this.handleChange} placeholder="minute" />
                    </div>
                  </div>
                  <input type="submit" className="btn" />
                </form>
              </div>
            </div>
          </div>
        
        )
    }
}

const mapSateToProps = (state) => ({
    currentUser: state.user.currentUser
});

export default connect(mapSateToProps)(UltimateMinivan);