import React from 'react'
import './Service.css';
import {Link} from 'react-router-dom';

const Service = ({CurrentUser}) => {
  if(!CurrentUser) return 'Loading';
  if(!CurrentUser.OIB) return 'Prijavili ste se kao korisnik te nemate pristup ovoj stranici!';
  else
   { return (
        <div className='main-container'>
           
           <h1>Odabir vozila i paketa</h1>
              <div className="row">
                <div className="column">

                    <Link to = {`/usluge/BasicSUV`}>
                        <div className="card" >
                        <h3>SUV</h3>
                        <p>Basic</p>
                        </div>
                    </Link>

                </div>
    
                <div className="column">
                <Link to = {`/usluge/BasicRegular`}>
                  <div className="card">
                    <h3>Regular</h3>
                    <p>Basic</p>
                  </div>
                </Link>
                </div>
            
            
                <div className="column">
                <Link to = {`/usluge/BasicMinivan`}>
                  <div className="card">
                    <h3>Minivan</h3>
                    <p>Basic</p>
                  </div>
                </Link>
                </div>
                <div className="column">
                <Link to = {`/usluge/BasicCargoTruck`}>
                  <div className="card">
                    <h3>Kombi vozilo</h3>
                    <p>Basic</p>
                    
                  </div>
                  </Link>
                </div>
            </div>

            <div className="row">
                <div className="column">

                    <Link to = {`/usluge/DeluxSUV`}>
                        <div className="card" >
                        <h3>SUV</h3>
                        <p>Delux</p>
                        </div>
                    </Link>

                </div>
    
                <div className="column">
                <Link to = {`/usluge/DeluxRegular`}>
                  <div className="card">
                    <h3>Regular</h3>
                    <p>Delux</p>
                  </div>
                  </Link>
                </div>
            
            
                <div className="column">
                <Link to = {`/usluge/DeluxMinivan`}>
                  <div className="card">
                    <h3>Minivan</h3>
                    <p>Delux</p>
                  </div>
                  </Link>
                </div>
                <div className="column">
                <Link to = {`/usluge/DeluxCargoTruck`}>
                  <div className="card">
                    <h3>Kombi vozilo</h3>
                    <p>Delux</p>
                    
                  </div>
                  </Link>
                </div>
            </div>

            <div className="row">
                <div className="column">

                    <Link to = {`/usluge/UltimateSUV`}>
                        <div className="card" >
                        <h3>SUV</h3>
                        <p>Ultimate</p>
                        </div>
                    </Link>

                </div>
    
                <div className="column">
                <Link to = {`/usluge/UltimateRegular`}>
                  <div className="card">
                    <h3>Regular</h3>
                    <p>Ultimate</p>
                  </div>
                  </Link>
                </div>
            
            
                <div className="column">
                <Link to = {`/usluge/UltimateMinivan`}>
                  <div className="card">
                    <h3>Minivan</h3>
                    <p>Ultimate</p>
                  </div>
                  </Link>
                </div>
                <div className="column">
                <Link to = {`/usluge/UltimateCargoTruck`}>
                  <div className="card">
                    <h3>Kombi vozilo</h3>
                    <p>Ultiamte</p>
                    
                  </div>
                  </Link>
                </div>
            </div>



            <div className="row">
                <div className="column">

                    <Link to = {`/usluge/PlatinumSUV`}>
                        <div className="card" >
                        <h3>SUV</h3>
                        <p>Platinum</p>
                        </div>
                    </Link>

                </div>
    
                <div className="column">
                <Link to = {`/usluge/PlatinumRegular`}>
                  <div className="card">
                    <h3>Regular</h3>
                    <p>Platinum</p>
                  </div>
                  </Link>
                </div>
            
            
                <div className="column">
                <Link to = {`/usluge/PlatinumMinivan`}>
                  <div className="card">
                    <h3>Minivan</h3>
                    <p>Platinum</p>
                  </div>
                  </Link>
                </div>
                <div className="column">
                <Link to = {`/usluge/PlatinumCargoTruck`}>
                  <div className="card">
                    <h3>Kombi vozilo</h3>
                    <p>Platinum</p>
                    
                  </div>
                  </Link>
                </div>
            </div>
        </div>
        
    )}
}


export default Service