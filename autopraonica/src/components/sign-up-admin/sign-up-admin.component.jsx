import React from 'react';
import { auth , createUserProfileDocument} from '../../firebase/firebase.utils';

import FormInput from '../form-input/form-input.component';

import { CustomButton} from '../../components';

class SignUp extends React.Component{
    constructor(){
        super();

        this.state = {
            displayName: '',
            email:'',
            password:'',
            confirmPassword:'',
            phonenumber:'',
            adress:'',
            admin:true,
            OIB:'',
            tvrtka:''
        };
    }

    handleSubmit = async event => {
        event.preventDefault();

        const {displayName,email,password,confirmPassword,phonenumber,adress,admin,OIB,tvrtka} = this.state;

        if(password !== confirmPassword){
            alert("passwords do not match");
        }
        else{
            try{
                const {user} = await auth.createUserWithEmailAndPassword(email,password);

                await createUserProfileDocument(user, {displayName,phonenumber,adress,admin,OIB,tvrtka});

                this.setState({
                    displayName: '',
                    email:'',
                    password:'',
                    confirmPassword:'',
                    phonenumber:'',
                    adress:'',
                    admin:true,
                    OIB:'',
                    tvrtka:''
                });
            } catch (error){
                console.error(error);
            }
        }
    }


    handleChange = event =>{
        const {name,value} = event.target;

        this.setState({[name]: value});
    }

    render(){
        const {displayName,email,password,confirmPassword,phonenumber,adress,OIB,tvrtka} = this.state;
        return(
            <div className='sign-up'>
            <h2 className='title'>REGISTRACIJA AUTOPRAONE</h2>
            <form className='sign-up-form' onSubmit = {this.handleSubmit}>
                <FormInput type='text' name='displayName' value= {displayName} onChange={this.handleChange} label='Display Name' required/>
                <FormInput type='text' name='OIB' value= {OIB} onChange={this.handleChange} label='OIB' required/>
                <FormInput type='text' name='tvrtka' value= {tvrtka} onChange={this.handleChange} label='Naziv tvrtke' required/>
                <FormInput type='text' name='phonenumber' value= {phonenumber} onChange={this.handleChange} label='Phone Number' required/>
                <FormInput type='text' name='adress' value= {adress} onChange={this.handleChange} label='Adress' required/>                
                <FormInput type='email' name='email' value= {email} onChange={this.handleChange} label='Email' required/>                
                <FormInput type='password' name='password' value= {password} onChange={this.handleChange} label='Password' required/>                
                <FormInput type='password' name='confirmPassword' value= {confirmPassword} onChange={this.handleChange} label='Confrim Password' required/>                
                <CustomButton type='submit'>SIGN UP</CustomButton>
            </form>
        </div>
        )
    }
}

export default SignUp;