import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { ReactComponent as Logo} from '../../assets/car.svg';
import { auth } from '../../firebase/firebase.utils';

import './header.styles.css';

const Header = ({ currentUser }) => (
    <div className = 'header'>
        <Link className='logo-container' to = "/">
            <Logo className='logo'/>
        </Link>
        {   
            currentUser ?
            (<div className='option' onClick= {()=> auth.signOut()}>
                Sign out 
            </div>)
            :
            ( <Link className='option' to = '/signin'>
                Sign In
            </Link>)
        }
        </div>
  
)

const mapSateToProps = (state) => ({
    currentUser: state.user.currentUser
});

export default connect(mapSateToProps)(Header);